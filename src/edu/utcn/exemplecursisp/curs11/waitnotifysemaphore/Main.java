package edu.utcn.exemplecursisp.curs11.waitnotifysemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(0);
        new Producer(semaphore).start();
        new Consumer(semaphore).start();
    }
}
