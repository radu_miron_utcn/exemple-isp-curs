package edu.utcn.exemplecursisp.curs11.waitnotifysemaphore;

import edu.utcn.exemplecursisp.curs10.ex1decoupling.FileHandler;
import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    private Semaphore semaphore;

    public Consumer(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(8000);
            System.out.println("The consumer will wait until the file is ready");

            semaphore.acquire();

            System.out.println(FileHandler.parseFile(ActivityUtils.FILE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
