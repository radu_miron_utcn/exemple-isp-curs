package edu.utcn.exemplecursisp.curs11.waitnotifysemaphore;

import edu.utcn.exemplecursisp.curs10.ex1decoupling.FileHandler;
import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Producer extends Thread {
    private Semaphore semaphore;

    public Producer(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        try {
            System.out.println("Start generating the file");
            FileHandler.generateFile(ActivityUtils.FILE_NAME);
            System.out.println("File is ready. Will notify the consumer");

            semaphore.release();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
