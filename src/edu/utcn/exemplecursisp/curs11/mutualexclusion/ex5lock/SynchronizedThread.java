package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex5lock;

import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class SynchronizedThread extends Thread {

    private Lock lock;

    public SynchronizedThread(Lock lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        lock.lock();
        ActivityUtils.doActivity("A2", 5);
        lock.unlock();

        ActivityUtils.doActivity("A3", 1);
    }
}
