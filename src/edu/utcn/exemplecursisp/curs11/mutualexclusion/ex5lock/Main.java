package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex5lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        new SynchronizedThread(lock).start();
        new SynchronizedThread(lock).start();
    }
}
