package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex4;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new SynchronizedThread().start();
        new SynchronizedThread().start();
    }
}
