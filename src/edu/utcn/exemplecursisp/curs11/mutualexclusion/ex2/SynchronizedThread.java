package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex2;

import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class SynchronizedThread extends Thread {

    private static Object lock = new Object();

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        synchronized (lock) {
            ActivityUtils.doActivity("A2", 5);
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
