package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex1;

import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class SynchronizedThread extends Thread {

    private Object lock;

    public SynchronizedThread(Object lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        synchronized (lock) {
            ActivityUtils.doActivity("A2", 5);
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
