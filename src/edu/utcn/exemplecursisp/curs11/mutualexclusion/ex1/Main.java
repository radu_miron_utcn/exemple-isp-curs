package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock = new Object();
        new SynchronizedThread(lock).start();
        new SynchronizedThread(lock).start();
    }
}
