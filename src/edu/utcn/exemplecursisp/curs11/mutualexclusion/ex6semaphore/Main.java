package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex6semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        new SynchronizedThread(semaphore).start();
        new SynchronizedThread(semaphore).start();
        new SynchronizedThread(semaphore).start();
    }
}
