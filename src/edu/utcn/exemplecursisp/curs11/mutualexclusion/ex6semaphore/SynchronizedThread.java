package edu.utcn.exemplecursisp.curs11.mutualexclusion.ex6semaphore;

import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class SynchronizedThread extends Thread {

    private Semaphore semaphore;

    public SynchronizedThread(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        try {
            semaphore.acquire();
            ActivityUtils.doActivity("A2", 5);
            semaphore.release();
        } catch (InterruptedException e) {
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
