package edu.utcn.exemplecursisp.curs11.util;

/**
 * @author Radu Miron
 * @version 1
 */
public class ActivityUtils {
    public static final String FILE_NAME = "testfiles/tobeparsed.txt";

    public static void doActivity(String activityName, int timeInSec) {
        System.out.println(String.format("[%s] Start executing activity %s", Thread.currentThread().getName(), activityName));

        try {
            Thread.sleep(timeInSec * 1000);
        } catch (InterruptedException e) {
        }

        System.out.println(String.format("[%s] Ended activity %s", Thread.currentThread().getName(), activityName));
    }

    public static synchronized void doCriticalActivity(String activityName, int timeInSec) {
        System.out.println(String.format("[%s] Start executing activity %s", Thread.currentThread().getName(), activityName));

        try {
            Thread.sleep(timeInSec * 1000);
        } catch (InterruptedException e) {
        }

        System.out.println(String.format("[%s] Ended activity %s", Thread.currentThread().getName(), activityName));
    }
}
