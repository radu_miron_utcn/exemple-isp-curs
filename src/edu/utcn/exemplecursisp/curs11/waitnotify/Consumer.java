package edu.utcn.exemplecursisp.curs11.waitnotify;

import edu.utcn.exemplecursisp.curs10.ex1decoupling.FileHandler;
import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    private Object monitor;

    public Consumer(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(8000);
            System.out.println("The consumer will wait until the file is ready");
            synchronized (monitor) {
                monitor.wait();
            }

            System.out.println(FileHandler.parseFile(ActivityUtils.FILE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
