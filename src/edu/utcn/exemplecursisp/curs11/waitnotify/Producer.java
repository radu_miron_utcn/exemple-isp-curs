package edu.utcn.exemplecursisp.curs11.waitnotify;

import edu.utcn.exemplecursisp.curs10.ex1decoupling.FileHandler;
import edu.utcn.exemplecursisp.curs11.util.ActivityUtils;

import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class Producer extends Thread {
    private Object monitor;

    public Producer(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {

        try {
            System.out.println("Start generating the file");
            FileHandler.generateFile(ActivityUtils.FILE_NAME);
            System.out.println("File is ready. Will notify the consumer");

            synchronized (monitor) {
                monitor.notify();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
