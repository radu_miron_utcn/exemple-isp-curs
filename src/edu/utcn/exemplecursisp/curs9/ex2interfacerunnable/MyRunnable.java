package edu.utcn.exemplecursisp.curs9.ex2interfacerunnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyRunnable implements Runnable {
    private int id;

    public MyRunnable(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(String.format("Thread %d - message number %d", id, i));
        }
    }

    public void start() {
        new Thread(this).start();
    }
}
