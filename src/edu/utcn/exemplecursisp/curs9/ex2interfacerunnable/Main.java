package edu.utcn.exemplecursisp.curs9.ex2interfacerunnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MyRunnable myRunnable1 = new MyRunnable(1);
        MyRunnable myRunnable2 = new MyRunnable(2);
        MyRunnable myRunnable3 = new MyRunnable(3);

        myRunnable1.start();
        myRunnable2.start();
        myRunnable3.start();

        System.out.println("this is the main thread");
    }
}
