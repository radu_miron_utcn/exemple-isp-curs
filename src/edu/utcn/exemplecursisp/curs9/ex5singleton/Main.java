package edu.utcn.exemplecursisp.curs9.ex5singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) {
        new Thread(() -> {
            System.out.println(Singleton.getInstance());
        }).start();

        new Thread(() -> {
            System.out.println(Singleton.getInstance());
        }).start();
    }
}
