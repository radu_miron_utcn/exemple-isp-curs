package edu.utcn.exemplecursisp.curs9.ex5singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Singleton {

    private static volatile Singleton instance;

    private Singleton() {
    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }

            instance = new Singleton();
        }

        return instance;
    }

    public void test1() {
        System.out.println("test 1");
    }

    public void test2() {
        System.out.println("test 2");
    }
}
