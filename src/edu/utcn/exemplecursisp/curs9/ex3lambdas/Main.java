package edu.utcn.exemplecursisp.curs9.ex3lambdas;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                for (int i1 = 0; i1 < 10; i1++) {
                    System.out.println(String.format("%s - message number %d", Thread.currentThread().getName(), i1));
                }
            }).start();
        }

        System.out.println(Thread.currentThread().getName() + " - this is the main thread's name");
    }
}
