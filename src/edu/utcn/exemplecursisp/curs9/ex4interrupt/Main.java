package edu.utcn.exemplecursisp.curs9.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        SlowExecThread slowExecThread = new SlowExecThread();
        Interrupter interrupter = new Interrupter(slowExecThread);

        slowExecThread.start();
        interrupter.start();
    }
}
