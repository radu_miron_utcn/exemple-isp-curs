package edu.utcn.exemplecursisp.curs9.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class SlowExecThread extends Thread {

    public SlowExecThread() {
        this.setName(SlowExecThread.class.getSimpleName());
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " does something");

        try {
            Thread.sleep(24 * 60 * 60 * 1000);
        } catch (InterruptedException e) {
            System.err.println(Thread.currentThread().getName() + " - was interrupted");
        }
    }
}
