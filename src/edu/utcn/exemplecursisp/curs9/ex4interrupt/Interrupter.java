package edu.utcn.exemplecursisp.curs9.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Interrupter extends Thread {
    private SlowExecThread slowExecThread;

    public Interrupter(SlowExecThread slowExecThread) {
        this.slowExecThread = slowExecThread;
    }

    @Override
    public void run() {
        System.out.println("The interrupter thread has started");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        System.out.println("Interrupt the SlowExecThread");
        slowExecThread.interrupt();
    }
}
