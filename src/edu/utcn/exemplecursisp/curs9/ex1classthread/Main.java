package edu.utcn.exemplecursisp.curs9.ex1classthread;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MyThread myThread1 = new MyThread(1);
        MyThread myThread2 = new MyThread(2);
        MyThread myThread3 = new MyThread(3);

        myThread1.start();
        myThread2.start();
        myThread3.start();

        System.out.println("this is the main thread");
    }
}
