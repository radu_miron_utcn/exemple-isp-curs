package edu.utcn.exemplecursisp.curs9.ex1classthread;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private int id;

    public MyThread(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(String.format("Thread %d - message number %d", id, i));

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
    }
}
