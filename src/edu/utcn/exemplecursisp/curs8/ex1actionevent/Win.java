package edu.utcn.exemplecursisp.curs8.ex1actionevent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame implements ActionListener {
    private JButton button1;
    private JButton button2;

    public Win() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(1, 2));
        this.setSize(200, 60);

        button1 = new JButton("Press 1");
        button1.addActionListener(this);

        button2 = new JButton("Press 2");
        button2.addActionListener(this);

        this.add(button1);
        this.add(button2);

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource().equals(button1)) {
            System.out.println("do something");
        } else if (actionEvent.getSource().equals(button2)) {
            System.out.println("do something else");
        }

        System.out.println(actionEvent.getActionCommand());
    }
}
