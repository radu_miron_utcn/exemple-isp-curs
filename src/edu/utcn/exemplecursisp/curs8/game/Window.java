package edu.utcn.exemplecursisp.curs8.game;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame implements KeyListener {
    public MainCharacter mainCharacter;

    public Window(MainCharacter mainCharacter, List<Enemy> enemies) {
        this.mainCharacter = mainCharacter;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(300, 300, 500, 500);
        setTitle("My Game");
        setLayout(null);

        add(mainCharacter);
        addKeyListener(this);

        for (Enemy enemy : enemies) {
            add(enemy);
        }

        setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        if (keyEvent.getKeyChar() == 'a' && this.mainCharacter.getX() > 0) {
            this.mainCharacter.setLocation(this.mainCharacter.getX() - 5, this.mainCharacter.getY());
        } else if (keyEvent.getKeyChar() == 'd' && this.mainCharacter.getX() < 460) {
            this.mainCharacter.setLocation(this.mainCharacter.getX() + 5, this.mainCharacter.getY());
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}
