package edu.utcn.exemplecursisp.curs8.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MainCharacter mainCharacter = new MainCharacter();
        mainCharacter.setBounds(230, 430, 40, 20);

        List<Enemy> enemies = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Enemy enemy = new Enemy();
            enemy.setBounds(i * 100 + 30, 0, 20, 20);
            enemies.add(enemy);
        }

        new Window(mainCharacter, enemies);

        while(true) {

            //todo: different speed for each enemy
            //todo: detect collisions
            //todo: once an enemy leaves the frame stop moving it
            //todo: use you imagination to implement a mini-game (multiple levels, score, pause/restart buttons etc.)

            for (Enemy enemy : enemies) {
                enemy.setLocation(enemy.getX(), enemy.getY() + 10);
            }

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
    }
}
