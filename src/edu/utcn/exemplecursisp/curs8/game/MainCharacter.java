package edu.utcn.exemplecursisp.curs8.game;

import java.awt.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class MainCharacter extends Component {
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLUE);
        g.fillRect(0, 0, 40, 20);
    }
}
