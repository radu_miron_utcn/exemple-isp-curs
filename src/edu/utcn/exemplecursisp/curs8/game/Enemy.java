package edu.utcn.exemplecursisp.curs8.game;

import java.awt.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Enemy extends Component {
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.RED);
        g.fillRect(0, 0, 20, 20);
    }
}
