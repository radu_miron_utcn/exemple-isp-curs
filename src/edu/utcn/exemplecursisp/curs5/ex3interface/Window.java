package edu.utcn.exemplecursisp.curs5.ex3interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        this.setTitle("My First Window");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(700);
        this.setBounds(x, y, 200, 200);

        JButton button = new JButton("Click!");
        ActionListener buttonHandler = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new Window();
            }
        };

        button.addActionListener(buttonHandler);

        this.add(button);

        this.setVisible(true);
    }
}
