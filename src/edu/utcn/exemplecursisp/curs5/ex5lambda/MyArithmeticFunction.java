package edu.utcn.exemplecursisp.curs5.ex5lambda;

/**
 * @author Radu Miron
 * @version 1
 */

@FunctionalInterface
public interface MyArithmeticFunction {
    int apply(int a, int b);
}
