package edu.utcn.exemplecursisp.curs5.ex5lambda;

/**
 * @author Radu Miron
 * @version 1
 */
public class Calculator {
    static int calculate(MyArithmeticFunction f, int a, int b) {
        return f.apply(a, b);
    }
}
