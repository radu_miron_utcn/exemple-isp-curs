package edu.utcn.exemplecursisp.curs5.ex5lambda;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("a+b=" + Calculator.calculate((a, b) -> a + b, 10, 3));
        System.out.println("a-b=" + Calculator.calculate((a, b) -> a - b, 10, 3));
        System.out.println("a*b=" + Calculator.calculate((a, b) -> a * b, 10, 3));
    }
}
