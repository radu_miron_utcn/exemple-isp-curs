package edu.utcn.exemplecursisp.curs5.ex4lambda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        this.setTitle("My First Window");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(700);
        this.setBounds(x, y, 200, 200);

        //x -> f(x)
        //x -> x + 1

        JButton button = new JButton("Click!");
        button.addActionListener(e -> {
            Window window = new Window();
            window.setTitle(e.getActionCommand());
        });

        this.add(button);

        this.setVisible(true);
    }
}
