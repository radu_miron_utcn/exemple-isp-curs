package edu.utcn.exemplecursisp.curs5.ex1interface;

/**
 * @author Radu Miron
 * @version 1
 * By default, all the interfaces' methods (and attributes) are public, not package.
 * The attributes in the interfaces are constants.
 */
public interface Interface1 {
    int A = 4;

    void met1();
    void met2();
}
