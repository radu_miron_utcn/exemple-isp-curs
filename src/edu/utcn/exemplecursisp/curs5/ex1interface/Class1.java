package edu.utcn.exemplecursisp.curs5.ex1interface;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 * <p>
 * A class can implement multiple interfaces.
 * If it's not abstract a class has to implement all the inherited methods that don't have an implementation.
 */
public class Class1 implements Interface1, Interface2 {
    @Override
    public void met1() {
        System.out.println("met1");
    }

    @Override
    public void met2() {
        System.out.println("met2");
    }

    @Override
    public void met3() {
        System.out.println("met3");
    }
}
