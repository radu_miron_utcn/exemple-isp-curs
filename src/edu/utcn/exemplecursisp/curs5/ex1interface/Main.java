package edu.utcn.exemplecursisp.curs5.ex1interface;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new Interface1() {
            @Override
            public void met1() {
            }

            @Override
            public void met2() {
            }
        };
    }
}
