package edu.utcn.exemplecursisp.curs5.ex1interface;

/**
 * @author Radu Miron
 * @version 1
 * An abstract class doesn't necessarely have to implement the methods that are inherited from the interfaces.
 */
public abstract class AbstractClass implements Interface2 {
}
