package edu.utcn.exemplecursisp.curs6.ex1collections.subex2set;

import java.util.*;

/**
 * @author radumiron
 * @version 1
 */
public class TreeSetExample {
    public static void main(String[] args) {
        Set<Human> humans = new HashSet<>();
        humans.add(new Human("Carles", "Puyol"));
        humans.add(new Human("Xavier", "Seven"));
        humans.add(new Human("Andy", "Test"));
        humans.add(new Human("Armin", "Cole"));
        print("HashSet", humans);
        System.out.println();

        // sort by first name - Human is comparable by first name
        Set<Human> humansSortedFistName = new TreeSet<>(humans);
        print("TreeSet - sorted by first name", humansSortedFistName);
        System.out.println();

        // sort by last name - with a comparator
        Set<Human> humansSortedLastName = new TreeSet<>((o1, o2) -> o1.lastName.compareTo(o2.lastName));
        humansSortedLastName.addAll(humans);
        print("TreeSet - sorted by last name", humansSortedLastName);
        System.out.println();
    }

    private static void print(String what, Set<Human> humans) {
        System.out.println(what);

        for (Human human : humans) {
            System.out.println(human);
        }
    }

    public static class Human implements Comparable<Human> {
        private String firstName;
        private String lastName;

        public Human(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Human human = (Human) o;
            return Objects.equals(firstName, human.firstName) &&
                    Objects.equals(lastName, human.lastName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(firstName, lastName);
        }

        @Override
        public String toString() {
            return firstName + " " + lastName;
        }

        @Override
        public int compareTo(Human o) {
            return this.firstName.compareTo(o.firstName);
        }
    }
}
