package edu.utcn.exemplecursisp.curs6.ex1collections.subex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class HashCollisions {
    public static void main(String[] args) {
        System.out.println("Aa".hashCode());
        System.out.println("BB".hashCode());

        Map<String, String> map = new HashMap<>();
        map.put("Aa", "val 1");
        map.put("Aa", "val 2");
        map.put("BB", "val 3");

        System.out.println(map.get("BB"));
        System.out.println();
    }
}
