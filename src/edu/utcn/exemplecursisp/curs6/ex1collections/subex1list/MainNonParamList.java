package edu.utcn.exemplecursisp.curs6.ex1collections.subex1list;

import java.util.ArrayList;
import java.util.List;

/**
 * @author radumiron
 * @version 1
 */
public class MainNonParamList {
    public static void main(String[] args) {
        // mixed object types - it's not such a good idea
        List list = new ArrayList();

        // ADD
        list.add("abcd");
        list.add(new Person("123", "John", "Doe", "12 Abc St, DC"));
        list.add(123);
        list.add(new Integer(12));
        list.add("deletable");

        // REMOVE
        list.remove(list.size() - 1);
        Object o = list.remove("abcd");

        // GET
        System.out.println(list.get(0).getClass());
        System.out.println(list.get(1).getClass());

        String s = (String) list.get(0); // type cast needed
        Person p = (Person) list.get(1); // type cast needed
        // etc.

        // sum the integers in the list
        int sum = 0;

        // ITERATE
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);

            if (obj instanceof Integer) {
                sum += (Integer) obj; // type cast needed
            }
        }
        // or
        for (Object obj : list) {
            if (obj instanceof Integer) {
                sum += (Integer) obj; // type cast needed
            }
        }
        // or
        int[] sumForLambda = {0};
        list.forEach(e -> {
            if (e instanceof Integer) {
                sumForLambda[0] += (Integer) e; // type cast needed
            }
        });

        System.out.println("sum=" + sum);

        // Anytime you find yourself writing code of the form "if the object is of type T1,
        // then do something, but if it's of type T2, then do something else," slap yourself.
        // (Effective C++, by Scott Meyers)
    }
}
