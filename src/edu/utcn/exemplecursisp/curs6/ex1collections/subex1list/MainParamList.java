package edu.utcn.exemplecursisp.curs6.ex1collections.subex1list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author radumiron
 * @version 1
 */
public class MainParamList {
    public static void main(String[] args) {
        // list of strings
        List<String> strings = new ArrayList<>();
        strings.add("ccc");
        strings.add("aaa");
        strings.add("bbb");
        String string1 = strings.get(1); // no type cast needed
        System.out.println("The first string in list: " + string1);

        System.out.println("Unsorted 'strings':");
        strings.forEach(s -> System.out.println(s));
        System.out.println();

        Collections.sort(strings);

        System.out.println("Sorted 'strings':");
        strings.forEach(s -> System.out.println(s));
        System.out.println();

        System.out.println("Shuffled 'strings':");
        Collections.shuffle(strings);
        System.out.println();
        strings.forEach(s -> System.out.println(s));

        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);

        int sum = 0;
        for (Integer i : integers) {
            sum += i; // no type cast needed
        }
        System.out.println("sum=" + sum);

        integers.stream()
                .reduce((a, b) -> a + b)
                .ifPresent(fSum -> System.out.println("functional sum = " + fSum));

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("123", "John", "Doe", "12 Abc St, DC"));
        persons.add(new Person("000", "Jane", "Doe", "12 Abc St, DC"));
        persons.add(new Person("456", "Jane", "Doe", "12 Abc St, DC"));
        persons.add(new Person("999", "Abigail", "Doe", "12 Abc St, DC"));
        Person person1 = persons.get(0); // no type cast needed
        System.out.println(person1);

        Collections.sort(persons);
        System.out.println();
        System.out.println("Sorted person list by id number:");
        persons.forEach(p -> System.out.println(p));

        Collections.sort(persons, (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName()));
        System.out.println();
        System.out.println("Sorted person list by first name:");
        persons.forEach(p -> System.out.println(p));
    }
}
