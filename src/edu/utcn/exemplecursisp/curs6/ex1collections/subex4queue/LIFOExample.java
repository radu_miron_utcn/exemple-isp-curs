package edu.utcn.exemplecursisp.curs6.ex1collections.subex4queue;

import java.util.Stack;

/**
 * @author radumiron
 * @version 1
 */
public class LIFOExample {
    public static void main(String[] args) {
        Stack<String> lifo = new Stack<>();
        lifo.push("1");
        lifo.push("1");
        lifo.push("2");
        lifo.push("3");

        int lifoSize = lifo.size();

        for (int i = 0; i < lifoSize; i++) {
            System.out.println(lifo.pop()); // alternatively you can use peek() - what's the difference?
        }
    }
}
