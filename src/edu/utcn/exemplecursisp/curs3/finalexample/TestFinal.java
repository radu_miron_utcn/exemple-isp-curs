package edu.utcn.exemplecursisp.curs3.finalexample;

/**
 * @author Radu Miron
 * @version 1
 */
public class TestFinal {
    public static void main(String[] args) {
        final int c = 5;
        // cannot change the value of a final variable
        //c = 6;
    }
}

final class BaseClass {
}

// the final classes cannot be extended
//class SubClass extends BaseClass {
//}

class BaseClass1 {
    public final void met() {
    }
}

class SubClass1 extends BaseClass1 {
    // the final methods cannot be overriden
//    public final void met(){
//    }
}