package edu.utcn.exemplecursisp.curs3.polymorphism;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Choose: 1. Dacia Logan; 2. VW T-Roc");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Car car;

        switch (choice) {
            case 1:
                car = new DaciaLogan("a good car");
                break;
            case 2:
                car = new VwTRoc("a german car");
                break;
            default:
                throw new IllegalArgumentException("Invalid choice");
        }

        car.start();
        car.go();
        car.stop();
    }
}
