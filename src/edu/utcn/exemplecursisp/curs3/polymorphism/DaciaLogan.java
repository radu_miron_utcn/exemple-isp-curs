package edu.utcn.exemplecursisp.curs3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class DaciaLogan extends Car {
    private String description;

    public DaciaLogan(String description) {
        super("Dacia Logan");
        this.description = description;
    }

    @Override
    public void go() {
        System.out.println("Dacia Logan goes fast");
    }
}
