package edu.utcn.exemplecursisp.curs3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private String name;

    public Car(String name) {
        this.name = name;
    }

    public void start() {
        System.out.println(name + " starts");
    }

    public void go() {
        System.out.println(name + " goes fast");
    }

    public void stop() {
        System.out.println(name + " stops");
    }
}
