package edu.utcn.exemplecursisp.curs3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class VwTRoc extends Car {
    private String description;

    public VwTRoc(String description) {
        super("VW T-Roc");
        this.description = description;
    }

    public void go() {
        System.out.println("VW T-Roc goes faster");
    }
}
