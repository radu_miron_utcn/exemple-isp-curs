package edu.utcn.exemplecursisp.curs3.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class ErasmusStudent extends Student {
    private String universityOfOrigin;

    public ErasmusStudent(String name, String regNum, String universityOfOrigin) {
        super(name, regNum);
        this.universityOfOrigin = universityOfOrigin;
    }

    public String getUniversityOfOrigin() {
        return universityOfOrigin;
    }

    public void setUniversityOfOrigin(String universityOfOrigin) {
        this.universityOfOrigin = universityOfOrigin;
    }
}
