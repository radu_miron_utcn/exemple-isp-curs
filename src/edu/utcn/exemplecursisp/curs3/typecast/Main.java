package edu.utcn.exemplecursisp.curs3.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Student student = new Student("John Doe", "123");
        Person person = student;
        Person person1 = (Person) student;

        student.getRegNum();
        student.getName();
        student.setRegNum("234");
        student.setName("John Doe 3rd");

//        person.getRegNum();
        person.getName();
//        person.setRegNum("234");
        person.setName("John Doe 3rd");

        Student student1 = (Student) person;
        System.out.println(student1.getName());

        Person person2 = new Person("Jane Doe");
        Student student2 = (Student) person2;

        ErasmusStudent erasmusStudent = new ErasmusStudent("Tim Miron", "321", "MU");
        Student student3 = erasmusStudent;
        Person person3 = erasmusStudent;
    }
}
