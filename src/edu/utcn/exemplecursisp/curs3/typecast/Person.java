package edu.utcn.exemplecursisp.curs3.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
