package edu.utcn.exemplecursisp.curs3.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class Student extends Person {
    private String regNum;

    public Student(String name, String regNum) {
        super(name);
        this.regNum = regNum;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }
}
