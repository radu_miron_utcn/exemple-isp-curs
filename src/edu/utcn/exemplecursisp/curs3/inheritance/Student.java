package edu.utcn.exemplecursisp.curs3.inheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public class Student extends Person {

    public Student(String name) {
        super(name);
    }

    @Override
    public void printName() {
        System.out.print("Student ");
        super.printName();
    }

    public void printName(int i) {
        System.out.print("Student ");
        super.printName();
        System.out.println(i);
    }
}
