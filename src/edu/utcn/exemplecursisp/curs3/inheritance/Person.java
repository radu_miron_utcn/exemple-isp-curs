package edu.utcn.exemplecursisp.curs3.inheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public class Person {
    String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    protected void printName() {
        System.out.println("Name: " + name);
    }

    protected void printName(String printTerminator) {
        System.out.println("Name: " + name + printTerminator);
    }
}
