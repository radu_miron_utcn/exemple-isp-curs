package edu.utcn.exemplecursisp.curs3.inheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new Student("Tim").printName();

        Person p = new Person("John");
        p.printName();
        p.printName(".");
        p.printName("|");
    }
}
