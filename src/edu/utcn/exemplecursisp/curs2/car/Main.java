package edu.utcn.exemplecursisp.curs2.car;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        System.out.println(car);
        Car car1 = new Car("Dacia", "Logan", "rosu", 1600);
        Car car2 = car1; // car1 and car2 are pointing to the same object, the one that was created above with new!

        Car car3 = new Car("VW", "T-Roc", "alb", 1900); // this is a new object

        System.out.println(car1);
        System.out.println(car2);
        System.out.println(car3);
        System.out.println(car);

        System.out.println(car1.toString() + " moves for " + car1.move() + " kms");

        car1.crash(car3);
        car3.crash(car1);

        System.out.println(Car.STANDARD_NUMBER_OF_WHEELS);

        Car.test();
    }
}
