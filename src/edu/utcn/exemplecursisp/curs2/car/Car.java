package edu.utcn.exemplecursisp.curs2.car;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    public static final int STANDARD_NUMBER_OF_WHEELS = 4;

    private String brand;
    private String model;
    private String color;
    protected int engineSize;

    public Car() {
    }

    public Car(String brand, String model, String color, Integer engineSize) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.engineSize = engineSize;
    }

    private void start() {
        System.out.println(this.toString() + " starts");
    }

    public int move() {
        start();
        return new Random().nextInt(100);
    }

    public void crash(Car theOtherCar) {
        System.out.println(this.toString() + " crashes into " + theOtherCar);
    }

    static void test(){
        System.out.println("test");
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", engineSize=" + engineSize +
                '}';
    }
}
