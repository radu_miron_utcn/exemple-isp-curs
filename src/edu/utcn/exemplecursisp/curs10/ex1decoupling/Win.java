package edu.utcn.exemplecursisp.curs10.ex1decoupling;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(400, 600);

        JTextField fileNameTF = new JTextField();
        fileNameTF.setBounds(20, 40, 360, 20);

        JTextArea results = new JTextArea();
        JScrollPane resultsScroll = new JScrollPane(results);
        resultsScroll.setBounds(20, 80, 360, 400);

        JButton button = new JButton("Parse File");
        button.setBounds(20, 500, 360, 20);
        button.addActionListener(ae -> {
            new Thread(() -> {
                // v1 - the better one
//                try {
//                    String fileName = fileNameTF.getText();
//
//                    if (validateFileName(fileName)) {
//                        results.append(FileHandler.parseFile(fileName));
//                    } else {
//                        results.append(fileName + " not a valid txt file\n");
//                    }
//                } catch (IOException ex) {
//                    results.append(ex.getMessage());
//                }

                //v2
                try {
                    String fileName = fileNameTF.getText();

                    if (validateFileName(fileName)) {
                        Map<Integer, Integer> occurrences = FileHandler.parseFileV2(fileName);

//                        synchronized (results) {
//                            results.append(fileName);
//                            results.append("\n");
//                            occurrences.entrySet().forEach(e -> {
//                                results.append(e.getKey() + "");
//                                results.append(" -> ");
//
//                                try {
//                                    Thread.sleep(100);
//                                } catch (InterruptedException ex) {
//                                }
//
//                                results.append(e.getValue() + "");
//                                results.append("\n");
//                            });
//                            results.append("\n");
//                        }

                        printMessages(fileName, results, occurrences);
                    } else {
                        results.append(fileName + " not a valid txt file\n");
                    }
                } catch (IOException ex) {
                    results.append(ex.getMessage());
                }
            }).start();
        });

        JButton button2 = new JButton("Print Thread Name");
        button2.setBounds(20, 540, 360, 20);
        button2.addActionListener(e -> results.append(Thread.currentThread().getName() + "\n"));

        add(fileNameTF);
        add(resultsScroll);
        add(button);
        add(button2);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Win();
    }

    private synchronized void printMessages(String fileName, JTextArea results, Map<Integer, Integer> occurrences) {
        results.append(fileName);
        results.append("\n");
        occurrences.entrySet().forEach(e -> {
            results.append(e.getKey() + "");
            results.append(" -> ");

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            results.append(e.getValue() + "");
            results.append("\n");
        });
        results.append("\n");
    }

    private boolean validateFileName(String fileName) {
        File file = new File(fileName);

        if (file.exists() && !file.isDirectory() && fileName.endsWith(".txt")) {
            return true;
        }

        return false;
    }
}
