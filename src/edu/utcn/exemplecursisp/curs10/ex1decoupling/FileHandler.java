package edu.utcn.exemplecursisp.curs10.ex1decoupling;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileHandler {
    public static void main(String[] args) throws IOException {
        generateFile("testfiles/test2.txt");
//        long t1 = System.currentTimeMillis();
//        System.out.println(parseFile("testfiles/test1.txt"));
//        long t2 = System.currentTimeMillis();
//        System.out.println(t2-t1);
    }

    public static void generateFile(String fileName) throws IOException {
        try (PrintWriter printWriter = new PrintWriter(fileName)) {
            for (int i = 0; i < 10000000; i++) {
                printWriter.println(new Random().nextInt(Integer.MAX_VALUE));
            }

            printWriter.flush();
        }
    }

    public static String parseFile(String fileName) throws IOException {
        Map<Integer, Integer> occurrences = new HashMap<>();
        Pattern pattern = Pattern.compile("\\d");

        Files.lines(Paths.get(fileName))
                .forEach(l -> {
                    Matcher matcher = pattern.matcher(l);

                    while (matcher.find()) {
                        Integer digit = Integer.parseInt(matcher.group(0));
                        Integer occurrence = occurrences.get(digit) != null ? occurrences.get(digit) + 1 : 1;
                        occurrences.put(digit, occurrence);
                    }
                });

        StringBuilder builder = new StringBuilder();
        builder.append(fileName).append("\n");
        occurrences.entrySet().forEach(e -> {
            builder.append(e.getKey()).append(" -> ").append(e.getValue()).append("\n");
        });
        builder.append("\n");

        return builder.toString();
    }

    public static Map<Integer, Integer> parseFileV2(String fileName) throws IOException {
        Map<Integer, Integer> occurrences = new HashMap<>();
        Pattern pattern = Pattern.compile("\\d");

        Files.lines(Paths.get(fileName))
                .forEach(l -> {
                    Matcher matcher = pattern.matcher(l);

                    while (matcher.find()) {
                        Integer digit = Integer.parseInt(matcher.group(0));
                        Integer occurrence = occurrences.get(digit) != null ? occurrences.get(digit) + 1 : 1;
                        occurrences.put(digit, occurrence);
                    }
                });

        return occurrences;
    }
}
