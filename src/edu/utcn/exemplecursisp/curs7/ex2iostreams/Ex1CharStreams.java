package edu.utcn.exemplecursisp.curs7.ex2iostreams;

import java.io.*;

/**
 * @author radumiron
 * @version 1
 */
public class Ex1CharStreams {
    private static final String FILE_DIR_RELATIVE_PATH = "testfiles";

    public static void main(String[] args) throws IOException {
        String sourceFile = FILE_DIR_RELATIVE_PATH + File.separator + "test.txt";
        String destinationFile = FILE_DIR_RELATIVE_PATH + File.separator + "test-copy.txt";
        copyFileWithTryCatchFinally(sourceFile, destinationFile);

        // of course you could use Files.copy() or Files.move() for a cleaner code
    }

    public static void copyFileWithTryCatchFinally(String sourceFilePath, String destinationFilePath) {
        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader(sourceFilePath);
            out = new FileWriter(destinationFilePath);

            int c;

            while ((c = in.read()) != -1) { // ‘c’ is the int value of a char
                out.write(c);
                System.out.print(((char) c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException ignored) {
            }
        }
    }

    // alternate implementation with 'try-with-resource'
    public void copyFileTryWithResources(String sourceFilePath, String destinationFilePath) {
        try (FileReader in = new FileReader(sourceFilePath);
             FileWriter out = new FileWriter(destinationFilePath)) {
            int c;

            while ((c = in.read()) != -1) { // ‘c’ is the int value of a char
                out.write(c);
                System.out.print(((char) c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
