package edu.utcn.exemplecursisp.curs7.ex2iostreams;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author radumiron
 * @version 1
 */
public class Ex2BinaryStreams {
    public static void main(String[] args) {
        String sourceFilePath = "testfiles/smiley.jpeg";
        String destinationFilePath = "testfiles/smiley-copy.jpeg";

        try (FileInputStream in = new FileInputStream(sourceFilePath);
             FileOutputStream out = new FileOutputStream(destinationFilePath)) {
            int c;
            while ((c = in.read()) != -1) { // ‘c’ is the int value of a byte
                out.write(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println();
        }
    }
}
