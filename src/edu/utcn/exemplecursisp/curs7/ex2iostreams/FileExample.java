package edu.utcn.exemplecursisp.curs7.ex2iostreams;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileExample {
    public static void main(String[] args) throws IOException {
        FileWriter fileWriter = new FileWriter(new File("a.txt"));

        for (int i = 0; i < 100; i++) {
            fileWriter.write("line no. " + i + "\n");
        }

        fileWriter.flush();
        fileWriter.close();
    }
}
