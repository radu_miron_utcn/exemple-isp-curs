package edu.utcn.exemplecursisp.curs7.ex3serialization.ver2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        PersonRepository personRepository;

        if(args.length == 0){
            throw new IllegalArgumentException("No argument was given");
        } else if(args[0].equals("file")){
            personRepository = new PersonRepositoryFileImpl();
        } else if(args[0].equals("array")){
            personRepository = new PersonRepositoryArrayImpl();
        } else {
            throw new IllegalArgumentException("Choose between 'file' and 'array'");
        }

        new Window(personRepository);
    }
}
