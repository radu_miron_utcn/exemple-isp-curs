package edu.utcn.exemplecursisp.curs7.ex3serialization.ver2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Radu Miron
 * @version 1
 */
public class PersonRepositoryFileImpl implements PersonRepository {
    private static final String REPO_PATH = "/tmp/storage";

    public PersonRepositoryFileImpl() {
        File storageDir = new File(REPO_PATH);

        if (!storageDir.exists() || storageDir.isFile()) {
            storageDir.mkdirs();
        }
    }

    public void create(Person person) {
        File personFile = new File(new File(REPO_PATH), person.getIdNum() + ".txt");
        try (FileWriter writer = new FileWriter(personFile)) {
            writer.write(person.toString());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
            // todo: handle this exception in a nicer way (show in a popup window)
        }
    }

    public Person read(String idNum) {
        File personFile = new File(new File(REPO_PATH), idNum + ".txt");
        try {
            String personString = Files.lines(personFile.toPath())
                    .findFirst()
                    .orElse(null);
            // todo parse the string (use substring [or regex])
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            // todo: handle this exception in a nicer way (show in a popup window)
        }

        return null;
    }

    public void update(Person person) {
        //todo: implement it!
    }

    public void delete(String idNum) {
        //todo: implement it!
    }
}
