package edu.utcn.exemplecursisp.curs7.ex3serialization.ver2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Person {
    private String idNum;
    private String firstName;
    private String lastName;

    public Person(String idNum, String firstName, String lastName) {
        this.idNum = idNum;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idNum='" + idNum + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
