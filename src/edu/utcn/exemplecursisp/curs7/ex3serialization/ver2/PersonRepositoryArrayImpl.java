package edu.utcn.exemplecursisp.curs7.ex3serialization.ver2;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class PersonRepositoryArrayImpl implements PersonRepository {
    private Person[] persons = new Person[500];
    private int counter = 0;

    public void create(Person person) {
        persons[counter++] = person;
        // todo: handle duplicates
    }

    public Person read(String idNum) {
//        for (Person person : persons) {
//            if (idNum.equals(person.getIdNum())){
//                return person;
//            }
//        }
//
//        return null;
        return Arrays.stream(persons)
                .filter(p -> p != null && idNum.equals(p.getIdNum()))
                .findFirst()
                .orElse(null);
    }

    public void update(Person person) {
        //todo: implement it!
    }

    public void delete(String idNum) {
        //todo: implement it!
    }
}
