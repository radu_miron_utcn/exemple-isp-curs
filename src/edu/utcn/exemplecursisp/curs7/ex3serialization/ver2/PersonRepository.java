package edu.utcn.exemplecursisp.curs7.ex3serialization.ver2;

/**
 * @author Radu Miron
 * @version 1
 */
public interface PersonRepository {

    public void create(Person person);

    public Person read(String idNum);

    public void update(Person person);

    public void delete(String idNum);
}
