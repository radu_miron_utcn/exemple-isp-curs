package edu.utcn.exemplecursisp.curs7.ex3serialization.ver1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        PersonRepository personRepository = new PersonRepository();
        new Window(personRepository);
    }
}
