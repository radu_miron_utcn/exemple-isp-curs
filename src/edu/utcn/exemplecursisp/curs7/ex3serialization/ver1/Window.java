package edu.utcn.exemplecursisp.curs7.ex3serialization.ver1;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    private PersonRepository personRepository;

    public Window(PersonRepository personRepository) {
        this.personRepository = personRepository;

        this.setLayout(null);
        this.setBounds(400, 400, 500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Person Manager");

        JLabel idNumL = new JLabel("ID Number");
        idNumL.setBounds(20, 20, 100, 20);

        JLabel firstNameL = new JLabel("First Name");
        firstNameL.setBounds(20, 60, 100, 20);

        JLabel lastNameL = new JLabel("Last Name");
        lastNameL.setBounds(20, 100, 100, 20);

        JTextField idNumTF = new JTextField();
        idNumTF.setBounds(130, 20, 320, 20);

        JTextField firstNameTF = new JTextField();
        firstNameTF.setBounds(130, 60, 320, 20);

        JTextField lastNameTF = new JTextField();
        lastNameTF.setBounds(130, 100, 320, 20);

        JTextArea detailsTA = new JTextArea();
        detailsTA.setBounds(20, 140, 430, 200);

        JButton createB = new JButton("Create");
        createB.setBounds(20, 400, 100, 20);
        createB.addActionListener(e -> {
            personRepository.create(
                    new Person(idNumTF.getText(), firstNameTF.getText(), lastNameTF.getText()));
            detailsTA.setText("Person " + idNumTF.getText() + " created!");
            idNumTF.setText("");
            firstNameTF.setText("");
            lastNameTF.setText("");
        });

        JButton readB = new JButton("Find");
        readB.setBounds(130, 400, 100, 20);
        readB.addActionListener(e -> {
            Person person = personRepository.read(idNumTF.getText());
            if (person != null) {
                detailsTA.setText(person.toString());
            } else {
                detailsTA.setText("Person not found!");
            }
        });

        JButton updateB = new JButton("Update");
        updateB.setBounds(240, 400, 100, 20);
        updateB.addActionListener(e -> System.out.println("update"));

        JButton deleteB = new JButton("Delete");
        deleteB.setBounds(350, 400, 100, 20);
        deleteB.addActionListener(e -> System.out.println("delete"));

        this.add(idNumL);
        this.add(firstNameL);
        this.add(lastNameL);
        this.add(idNumTF);
        this.add(firstNameTF);
        this.add(lastNameTF);
        this.add(detailsTA);
        this.add(createB);
        this.add(readB);
        this.add(updateB);
        this.add(deleteB);
        this.setVisible(true);
    }
}
