package edu.utcn.exemplecursisp.curs12.ex1singleton;

import java.io.*;
import java.util.List;

/**
 * @author Radu Miron
 */
public class Ex4SingletonEnum {
    public static void main(String[] args) {
        FileWriterEnum fileWriter1 = FileWriterEnum.INSTANCE;
        fileWriter1.setFileName("test1");

        FileWriterEnum fileWriter2 = FileWriterEnum.INSTANCE;
        fileWriter2.setFileName("test2");

        System.out.println("After changing fileWriter2.fileName");
        System.out.println("fileWriter1.fileName: " + fileWriter1.getFileName());
        System.out.println("fileWriter2.fileName: " + fileWriter2.getFileName());
        System.out.println("----------------------------------------------------------------");

        // now lets try to serialize it
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("fileWriterEnum.dat"))) {
            oos.writeObject(fileWriter1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // and deserialize
        FileWriterEnum fileWriterEnumDeserialized = null;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("fileWriterEnum.dat"))) {
            fileWriterEnumDeserialized = (FileWriterEnum) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("After the deserialization");
        System.out.println("fileWriterEnumDeserialized.fileName: " + fileWriterEnumDeserialized.getFileName());

        // TODO: Explain the following:
        // Some say the field 'fileName' is not serialized (i.e. it should be null after the deserialization)
        // We can clearly see that it is not null!
        // Why?
        // Modify the example above to prove that 'some' are right!
    }
}

enum  FileWriterEnum {
    INSTANCE;

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void writeLines(List<String> lines) {
        //todo: implement
    }
}